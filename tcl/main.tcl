################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {
    variable Config

    # Configuration from package
    parseVhdlConfigFile Config "../hdl/pkg_combpm.vhd"

    # TODO add quad to configuration

}

# ==============================================================================
proc setSources {} {
  variable Vhdl

  # Generate VHDL package with modversion
  genModVerFile VHDL ../hdl/pkg_combpm_version.vhd

  lappend Vhdl {"../hdl/pkg_combpm_version.vhd" "VHDL 2008"}
  lappend Vhdl {"../hdl/pkg_combpm.vhd" "VHDL 2008"}
  lappend Vhdl {"../hdl/combpm_protocol_electron.vhd" "VHDL 2008"}
  lappend Vhdl {"../hdl/top_combpm_electron.vhd" "VHDL 2008"}
  lappend Vhdl {"../hdl/combpm_packet_filter.vhd" "VHDL 2008"}

}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    addAddressSpace AddressSpace "combpm" RDL {} ../rdl/combpm.rdl
}

# ==============================================================================
proc doOnCreate {} {

  # This module is only for vivado tool. End here for another tooltype.
  if {$::fwfwk::ToolType ne "vivado"} {
      puts "\n[ERROR] This module is only configured to work with Vivado"
      exit -1
  }

  # Create GT wizard IP
  set_property part ${::fwfwk::FpgaPart} [current_project]
  set_property target_language VHDL [current_project]
  source combpm_gtwizard.tcl

  variable Vhdl
  addSources Vhdl
}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
}
