library ieee;
USe ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library desy;
use desy.ram_tdp;

use work.pkg_combpm.C_W_BPMID;
use work.pkg_combpm.C_W_TDEST;

entity combpm_packet_filter is
    generic(
        G_W_ADDR_TABLE : natural;
        G_W_TDATA : natural
    );
    port(
        axis_clk                         : in std_logic;
        axi_clk                          : in std_logic;
        axis_rst_n                       : in std_logic;

        -- AXIS SLAVE INTERFACE
        s_axis_tvalid                    : in std_logic;
        s_axis_tdata                     : in std_logic_vector(G_W_TDATA-1 downto 0);
        s_axis_tuser_bpmid               : in std_logic_vector(C_W_BPMID-1 downto 0);

        -- AXIS MASTER INTERFACE
        m_axis_tvalid                    : out std_logic;
        m_axis_tdata                     : out std_logic_vector(G_W_TDATA-1 downto 0);
        m_axis_tdest                     : out std_logic_vector(C_W_TDEST-1 downto 0);

        -- Table configuration interface
        pi_table_en                      : in std_logic;
        pi_table_we                      : in std_logic;
        pi_table_addr                    : in std_logic_vector(G_W_ADDR_TABLE-1 downto 0);
        pi_table_data                    : in std_logic_vector(7 downto 0);
        po_table_data                    : out std_logic_vector(7 downto 0)

    );
end combpm_packet_filter;


architecture rtl of combpm_packet_filter is

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal r1_tdata     : std_logic_vector(G_W_TDATA-1 downto 0);
    signal r2_tdata     : std_logic_vector(G_W_TDATA-1 downto 0);
    signal r_bpmid             : std_logic_vector(C_W_BPMID-1 downto 0);

    signal table_data : std_logic_vector(7 downto 0);

    signal tvalid_r : std_logic_vector(1 downto 0);

begin

    ----------------------
    -- STREAM REGISTERS --
    ----------------------
    p_main: process(axis_clk, axis_rst_n)
    begin
        if axis_rst_n = '0' then
            r1_tdata    <= (others => '0');
            r2_tdata    <= (others => '0');
            r_bpmid     <= (others => '0');
            tvalid_r     <= (others => '0');

        elsif rising_edge(axis_clk) then
            -- Register input packet
            if s_axis_tvalid = '1' then
                r1_tdata    <= s_axis_tdata;
                r_bpmid     <= s_axis_tuser_bpmid;
            end if;

            tvalid_r(tvalid_r'left downto 1) <= tvalid_r(tvalid_r'left-1 downto 0);
            tvalid_r(0) <= s_axis_tvalid;

            r2_tdata <= r1_tdata;

        end if;
    end process;

    -----------------
    -- AXIS OUTPUT --
    -----------------
    m_axis_tdest        <= std_logic_vector(resize(unsigned(table_data(6 downto 0)), C_W_TDEST));
    m_axis_tdata        <= r2_tdata;
    m_axis_tvalid       <= tvalid_r(tvalid_r'left) and table_data(7);

    ------------------
    -- FILTER TABLE --
    ------------------
    -- Port A is read write from AXI controller, Port B is read only from logic
    inst_phase_table: entity desy.ram_tdp
    generic map(
        G_ADDR  => G_W_ADDR_TABLE,
        G_DATA  => 8
    )
    port map(
        pi_clk_a    => axi_clk,
        pi_en_a     => pi_table_en,
        pi_we_a     => pi_table_we,
        pi_addr_a   => pi_table_addr,
        pi_data_a   => pi_table_data,
        po_data_a   => po_table_data,
        pi_clk_b    => axis_clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => r_bpmid(G_W_ADDR_TABLE-1 downto 0),
        pi_data_b   => (others => '0'),
        po_data_b   => table_data
    );

end architecture rtl;
