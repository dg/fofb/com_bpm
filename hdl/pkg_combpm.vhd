

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_combpm is


    constant C_W_ADDR_TABLE : natural := 8;

    constant C_W_TDEST      : natural := 7;
    constant C_W_POS        : natural := 32;
    constant C_W_BPMID      : natural := 16;
    constant C_W_SEQ        : natural := 16;
    constant C_W_MCTS       : natural := 40;

    -------------------------
    -- GT WIZARD COMPONENT --
    -------------------------
    COMPONENT combpm_gtwizard
      PORT (
        gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_qpll1lock_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_reset_qpll1reset_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        qpll0clk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        qpll0refclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        qpll1clk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        qpll1refclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        rx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxbufreset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxcommadeten_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxmcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxpcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        tx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        txctrl0_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        txctrl1_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        txctrl2_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxbufstatus_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxbyterealign_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxcdrlock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxclkcorcnt_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        rxcommadet_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        rxctrl0_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        rxctrl1_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        rxctrl2_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        rxctrl3_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
      );
    END COMPONENT;

end package;
