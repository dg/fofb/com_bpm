`include "combpm.vh" // Auto generated from FWK

/* default values of defined variables */
`ifndef C_ID
`define C_ID 0x507E1710
`endif

addrmap combpm {
    name="BPM protocol decoder controller";
    desyrdl_interface = "AXI4L";


    reg {
        desc="Module Identification Number";
        default sw = r; default hw = r;
        field {} data[32] = `C_ID;
    } ID @0x00;

    reg {
        desc="Module version.";
        field {hw=w;sw=r;} data[32];
    } VERSION @0x04;

    reg {
        desc="SFP module status";
        desyrdl_data_type="bitfields";

        field {desc="RX lost signal";hw=w;sw=r;
        } RXLOS;
        field {desc="Module is absent";hw=w;sw=r;
        } MODABS;
    } SFP;

    reg {
        desc="GT transceivers status";
        desyrdl_data_type="bitfields";
        default sw = r; default hw = w;
        field {desc="Powergood signal";} POWERGOOD;
        field {desc="PLL lock signal";} QPLLLOCK;
        field {desc="RX clk active signal";} RXCLKACTIVE;
        field {desc="RX CDR lock signal";} RXCDRLOCK;
        field {desc="RX reset done signal";} RXRESETDONE;
        field {desc="RX byte is aligned signal";} RXBYTEISALIGNED;
        field {desc="RX byte realign signal";} RXBYTEREALIGN;
        field {desc="RX comma detected signal";} RXCOMMADET;
    } GT_STATUS;

    reg {
        desc="GT transceivers control";
        desyrdl_data_type="bitfields";
        default sw = rw; default hw = r;
        field {desc="RX comma detection enable signal";} RXCOMMADETEN = 1;
        field {desc="Reset RX datapath";} RXRSTDATAPATH = 1;
        field {desc="Reset RX PLL and datapath";} RXRSTPLLDATAPATH = 1;
    } GT_CONTROL;

    reg {
        desc="BPM protocol status and control";
        desyrdl_data_type="bitfields";
        default sw = r; default hw = w;
        field {desc="Frame error";} FRAMEERROR;
        field {desc="Sequence frame count mismatch";} SEQFRAMECNTERROR;
        field {desc="Sequence frame discontinuity";} SEQFRAMEDISCONT;
    } PROTOCOL_ERROR;

    reg  {
        default sw = rw; default hw = r;
        field {desc="Reset error flags";} SOFTRESET;
    } RESET_ERROR;

    reg  {
        default sw = rw; default hw = r;
        field {desc="Soft reset";} SOFTRESET;
    } RESET;

    reg {
        desc="BPM protocol valid frame counters";
        field {hw=w;sw=r;} data[32];
    } VALIDFRAMECNT;

    reg {
        desc="BPM protocol invalid frame counters";
        field {hw=w;sw=r;} data[32];
    } INVALIDFRAMECNT;

    reg {
        desc="BPM protocol valid frame rate";
        field {hw=w;sw=r;} data[32];
    } VALIDFRAMERATE;

    reg {
        desc="BPM protocol invalid frame rate";
        field {hw=w;sw=r;} data[32];
    } INVALIDFRAMERATE;

    reg {
        desc="BPM protocol frame sequence";
        field {hw=w;sw=r;} data[16];
    } FRAMESEQ;

    external mem {
        desc = "BPM filter table";
        memwidth = 32;
        mementries = 2**`C_W_ADDR_TABLE;
    } FILTERTABLE;

    reg {
        desc="Sequence numbering offset";
        desyrdl_data_type="int16";
        field {sw = rw; hw = r;} data[16]=0;
    } SEQ_OFFSET;

};
